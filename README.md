# ST Demo

## Information

This is a demo project that sets up Wordpress with 2 web servers (by default) and a database server in AWS. You'll set some things up locally and in AWS to run it. It should run in a free tier. or if not it will cost pennies if running for just a few hours.

**NOTE THIS IS A DEMO AND IS NOT SET UP SECURELY. DO NOT USE FOR PRODUCTION.**


## Usage

### Required Tools

The tools below are required to execute this. Installation commands are provided for the Mac (in the Terminal app), but directions can be found on the web for other OS's

* [homebrew](https://brew.sh): `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"`
* [ansible](https://github.com/ansible/ansible): `brew install ansible`
* [terraform](https://www.terraform.io): `brew install terraform`
* [awscli](https://aws.amazon.com/cli/): `brew install awscli`

### Required Credentials

An SSH key is required to connect to the servers. If this is already set up (run `ls ~/.ssh` and if you see a couple of files starting with `id_` you are set), you can skip this:
`ssh-keygen -t ed25519 -C <your name>` and hit return a couple of times.

AWS credentials are required to create the servers. Follow the `Access key ID and secret access key` and `Quick configuration with aws configure` steps [on the AWS CLI docs](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-quickstart.html) to set this up. Note that region should be `us-east-1` for the purposes of this demo

### Execution

A. Terraform
1. Go to the `terraform` directory in the Terminal.
2. Run `terraform init`. This will prepare terraform.
3. Run `terraform apply`. Type `yes` when prompted. This will create the servers and all necessary connections.
4. When its done running, note the entry after `elb_dns_name` at the end: this is the web site to connect to Wordpress. Save this for later.

B. Ansible
1. Go to the `ansible` directory in the Terminal.
2. Run `ansible-playbook -i aws_ec2.yml -b -u ubuntu provision.yml`. This will provision the servers.

C. Viewing
1. After about 30 seconds (for how long the AWS ELB needs to recognize everything is accessible) you can go to the `elb_dns_name` site from above in a browser. It will be something like this: `http://stdemo-123456789.us-east-1.elb.amazonaws.com`.
2. You can now set up Wordpress, with a title, username, password, and email(which doesn't matter- put in test@test.com). You can then log in and create a post. Upload a picture: its designed to put the picture on all servers.
3. You can go to `http://stdemo-123456789.us-east-1.elb.amazonaws.com/test.html` (putting in the `elb_dns_name` from above) and see all the web servers change as different ones serve the site.

D. Web Server Count
1. You can change the number of web servers in the `terraform/aws-infrastructure.auto.tfvars` file, by changing `instance_count` under the `aws_instances->web` section.
2. Go to the `terraform` directory.
3. Run `terraform apply`
4. Go to the `ansible` directory.
5. Run `ansible-playbook -i aws_ec2.yml -b -u ubuntu provision.yml`
6. Reload the `test.html` page and notice a new host there.

E. Cleaning up
1. Go to the `terraform` directory.
2. Run `terraform destroy`. Type `yes` when prompted. This will remove all servers and configs from this demo from AWS.

## Technical Information

Terraform for the infrastructure provisioning: This allows maintaining state. The variable definitions are in `aws-infrastructure-vars.tf`, the variable values are in `aws-infrastructure.auto.tfvars`, and the provisioning config is in `aws-infrastructure.tf`. The config uses the `aws` provider, the AWS `vpc`, `elb_http`, & `ec2_instances` modules, and the `aws_security_group` & `aws_key_pair` resources for easiest configuration

Ansible for the configuration provisioning: This allows easy and repeatable configuration of the servers. The config uses the `aws_ec2` plugin to enable dynamic inventory. It uses a single playbook, with multiple hosts for quick and easy running. It uses four roles for easy code reuse: `nginx` (web server), `php` (web code required with Wordpress), `mysql` (Percona database server) `gluster` (allows decentralized file sharing for Wordpress uploads), and `wordpress` (downloads, installs, and configures all web and database settings).

AWS for the infrastructure: this is a popular cloud provider. It uses EC2 instances for the servers, VPC for allow private database and file sharing traffic, and ELB to load balance between the web instances. Note that this could easily be swapped out for Google Cloud, Azure, or even Digital Ocean with only a little change
