<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress');

/** MySQL database username */
define('DB_USER', 'wordpress');

/** MySQL database password */
define('DB_PASSWORD', '{{ lookup('password', '/tmp/passwordfile') }}');

/** MySQL hostname */
define('DB_HOST', '{{ database_private_ip }}');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Jcq,2|S0-dybY1C6K1uA,];sHnsiw0gY1^Qkk|bn{ntt|}p5Lz!Amu?&4y-=EU%4');
define('SECURE_AUTH_KEY',  '4M!NE/bu.m1>jn-&greV`>RIDb2N?^ltY k__w_r=Z]t8}6Y<cs/zixc$h<RB_kx');
define('LOGGED_IN_KEY',    '|0$}hp,ktf +AwwL*B6:X^B/S:5=I~=Qo#1p9jO)p)tjBx<3%V9.UHNd2rg`DfIb');
define('NONCE_KEY',        'uEzi<|X{qHy2H+Cm8{:iWcY(|RFIFi_cNAUq0`,L{-xuLgJ~M>k5kT{B-tqy:`)1');
define('AUTH_SALT',        'XMDTtPjxyx< `@^p$xiy>YD;%V&/j_AUj;_!Gru S~r;kX}_5&Z#C-]&K/o={ Id');
define('SECURE_AUTH_SALT', 'Bk(x)LSl[{EWc GxBlLG6FXo!i!iqpAd<+%h^_1%FFsos~FG[E&.=*^i+yVU@71j');
define('LOGGED_IN_SALT',   '_k3j3,|G75+97}$3o+V<=G)ra*TP!C.3OJhS|1n8q!TQ7mXa-.M3i`O6>o2M$n!F');
define('NONCE_SALT',       'lQ}At#..U^5rh|^AeS!FW`q/(av:_f/`9k)_ce5)O-+2n0<$slDgTN->uYlS>q;F');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/** Custom Additions */

/** Disable editing files */
define('DISALLOW_FILE_EDIT', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
