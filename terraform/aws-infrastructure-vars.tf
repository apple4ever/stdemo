variable "aws_region" {
  description = "AWS Region"
}

variable "vpc_block" {
  description = "VPC network block"
}

variable "aws_availability_zones" {
  description = "VPC availability zones"
  type        = list
}

variable "public_subnet" {
  description = "Public subnet"
  type        = list
}

variable "private_subnet" {
  description = "Private subnet"
  type        = list
}

variable "ubuntu2004amis" {
  type = map(string)
}

variable aws_instances {
  description = "Map of instances to configuration"
  type = map(any)
}
