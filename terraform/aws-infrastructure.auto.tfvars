# Use US East
# Can change to other regions, but make sure to have aws_availability_zones below
aws_region = "us-east-1"
aws_availability_zones = ["us-east-1a", "us-east-1b", "us-east-1c", "us-east-1d", "us-east-1e", "us-east-1f"]

# Big block of internal IPs to use
vpc_block = "10.222.0.0/16"

# Public and private subnets for AZs, this are just logicalvpc_block = "10.222.0.0/16"
public_subnet  = ["10.222.1.0/24", "10.222.2.0/24", "10.222.3.0/24", "10.222.4.0/24", "10.222.5.0/24", "10.222.6.0/24"]
private_subnet = ["10.222.21.0/24", "10.222.22.0/24", "10.222.23.0/24", "10.222.24.0/24", "10.222.25.0/24", "10.222.26.0/24"]

# Could search for this, but easier for this demo to just put it in here
ubuntu2004amis = {
  "us-east-1" = "ami-0758470213bdd23b1"
}

# Increase instance_count under web below to add more web servers
aws_instances = {
  web = {
    name = "web"
    instance_count = 2
    tags = {
      Terraform = "true"
      Role = "web"
    }
  },
  database = {
    name = "database"
    instance_count = 1
    tags = {
      Terraform = "true"
      Role = "database"
    }
  }
}
