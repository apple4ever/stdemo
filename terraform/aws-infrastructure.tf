# AWS Specific Infrastructure Configuration

## Providers

terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
}

provider "aws" {
  region = var.aws_region
}

## Modules

### Setup VPC in our region with our subnets

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name            = "stdemo"
  cidr            = var.vpc_block
  azs             = var.aws_availability_zones
  public_subnets  = var.public_subnet
  private_subnets = var.private_subnet

  create_igw         = true
}

### Setup ELB with the web servers as the backend

module "elb_http" {
  source  = "terraform-aws-modules/elb/aws"

  name = "stdemo"

  subnets         = module.vpc.public_subnets
  security_groups = [aws_security_group.stdemo.id]
  internal        = false

  listener = [
    {
      instance_port     = "80"
      instance_protocol = "HTTP"
      lb_port           = "80"
      lb_protocol       = "HTTP"
    }
  ]

  health_check = {
    target              = "HTTP:80/license.txt"
    interval            = 30
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 5
  }

  number_of_instances = var.aws_instances["web"]["instance_count"]
  instances           = [ for aws_instance in sort(keys(var.aws_instances)) : module.ec2_instances[aws_instance].id if aws_instance == "web" ][0]

}

### Setup our instances, both web and database

module "ec2_instances" {
  source = "terraform-aws-modules/ec2-instance/aws"

  for_each = var.aws_instances

  name = each.key
  instance_count = each.value.instance_count

  ami                    = var.ubuntu2004amis[var.aws_region]
  instance_type          = "t2.micro"
  vpc_security_group_ids = [aws_security_group.stdemo.id]
  subnet_id              = module.vpc.public_subnets[0]

  key_name = aws_key_pair.personal.key_name

  tags = each.value.tags
}

## Resources

### Setup our Security Groups to only allow ports we need

resource "aws_security_group" "stdemo" {
  name   = "stdemo"
  vpc_id = module.vpc.vpc_id
  #### SSH
  ingress {
      from_port = 22
      to_port   = 22
      protocol  = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }
  #### MySQL
  ingress {
      from_port = 3306
      to_port   = 3306
      protocol  = "tcp"
      cidr_blocks = ["10.222.0.0/16"]
  }
  #### HTTP
  ingress {
      from_port = 80
      to_port   = 80
      protocol  = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
  }
  #### Gluster
  ingress {
      from_port = 111
      to_port   = 111
      protocol  = "tcp"
      cidr_blocks = ["10.222.0.0/16"]
  }
  ingress {
      from_port = 24007
      to_port   = 24008
      protocol  = "tcp"
      cidr_blocks = ["10.222.0.0/16"]
  }
  ingress {
      from_port = 38465
      to_port   = 38466
      protocol  = "tcp"
      cidr_blocks = ["10.222.0.0/16"]
  }
  ingress {
      from_port = 49152
      to_port   = 49200
      protocol  = "tcp"
      cidr_blocks = ["10.222.0.0/16"]
  }
  #### Out
  egress {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "stdemo"
  }
}

### Setup SSH key to allow remote SSH

resource "aws_key_pair" "personal" {
  key_name   = "personal"
  public_key = file("~/.ssh/id_rsa.pub")
}

## Outputs

### Output public ips for troubleshooting

output "ec2_instance_public_ips" {
  description = "Public IP addresses of EC2 instances"
  value       = { for aws_instance in sort(keys(var.aws_instances)) : aws_instance => module.ec2_instances[aws_instance].public_ip }
}

### Output ELB DNS name to access Wordpress

output "elb_dns_name" {
  description = "DNS name of ELB"
  value       = module.elb_http.this_elb_dns_name
}
